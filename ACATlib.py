import numpy as np
import pandas as pd
import pickle
import sys
import math
#-----------------------------------
#k_elimination rate constant
def calc_elim_rate_const(c,ninterp=200):
    #from popkat pk.calc
    C = np.asanyarray(c)
    t_vector = np.linspace(0,24,len(C))
    # eliminate 'bad' values
    inds = (C > 0)
    t_vector, C = t_vector[inds], C[inds]
    lnC = np.log(C)
    # linear fit to last points

    linear = 1
    slope, intercept = np.polyfit(t_vector[-200:], lnC[-200:], linear)
    k_eli = -slope
    return k_eli
# finding t_max
def t_max_calc(x):
    X = np.asanyarray(x)
    t_vector = np.linspace(0,24,len(X))
    c_max = np.argmax(X)
    tmax = t_vector[c_max]
    return tmax
#-------------------
#calculating AUC_INF
def AUC(c):
    C = np.asanyarray(c)
    t_vector = np.linspace(0,24,len(C))
    return np.trapz(C,t_vector)
#------------------
#finding AUC infinity
def AUC_INF(c,inf = False):
    C = np.asanyarray(c)
    t_vector = np.linspace(0,24,len(C))
    k_eli = calc_elim_rate_const(t_vector, C)
    AUC_inf = (C[-1]/k_eli) if inf else 0
    return np.trapz(C,t_vector)+AUC_inf
#-------------------
#mean residense time for oral addministration
def MRT_oral(c):
    C = np.asanyarray(c)
    t_vector = np.linspace(0,24,len(C))
    tc = t_vector*C
    AUMC = np.trapz(tc,t_vector)
    AUC = np.trapz(tc,t_vector)
    MIT = np.trapz(C,t_vector)
#Mean input time (MIT)
    MRT = AUMC/AUC
    return MRT
#-------------------
def PKB_calc(c):
    PKA = pd.to_numeric(c,downcast ='float')
    PKB = 14-PKA
    return PKB
#-----------------------

def ACAT_predictcmax(V):
        v = np.asanyarray(V)
        logP_ow = v[0]
        pKa =v[1]
        fup =v[2]
        BP =v[3]
        pc = partition_coeffs(logP_ow, pKa, fup, BP)
        Kpuu_colon = fup/pc
        Vmax_met_vitro = v[4]
        BDM =v[5]
        Km_met_vitro = v[6]
        Kpuu_liver =fup/pc
        dose = v[7]
        V =[Kpuu_colon , Vmax_met_vitro , BDM, Km_met_vitro, Kpuu_liver, dose]
        V = np.asanyarray(V) 
        RF_clf_cmax = pickle.load(open('clf_cmax.sav','rb'))
        C_max = RF_clf_cmax.predict([Kpuu_colon, Vmax_met_vitro,BDM, Km_met_vitro,Kpuu_liver,dose])
        return C_max
    #------------------------
    
def ACAT_predicttmax(V):
        v = np.asanyarray(V)
        logP_ow = v[0]
        pKa =v[1]
        fup =v[2]
        BP =v[3]
        pc = partition_coeffs(logP_ow, pKa, fup, BP)
        Vmax_met_vitro = v[4]
        BDM =v[5]
        Km_met_vitro = v[6]
        dose = v[7]
        Kpuu_colon = fup/pc
        Kpuu_liver =fup/pc
        RF_clf_tmax = pickle.load(open('clf_tmax.sav','rb'))
        t_max = RF_clf_tmax.predict([ Kpuu_colon, BDM, Kpuu_liver, Vmax_met_vitro, Km_met_vitro])
        return t_max
    #-------------------------

def ACAT_predictAUC(V):
        v = np.asanyarray(V)
        logP_ow = v[0]
        pKa =v[1]
        fup =v[2]
        BP =v[3]
        pc = partition_coeffs(logP_ow, pKa, fup, BP)
        Vmax_met_vitro = v[4]
        BDM =v[5]
        Km_met_vitro = v[6]
        dose = v[7]
        Kpuu_colon = fup/pc
        Kpuu_liver =fup/pc
        RF_clf_AUC = pickle.load(open('clf_AUC.sav','rb'))
        AUC = RF_clf_AUC.predict([Kpuu_colon,BDM, Km_met_vitro,Vmax_met_vitro, Kpuu_liver])
        Cl =1/AUC
        return Cl
#---------------------------
def ACAT_Sorter(v1,v2):
    V1= np.asanyarray(v1)
    V2= np.asanyarray(v2)
    V1_sorted = np.sort(V1)
    sorter = np.argsort(V1)
    V2_sorted =V2[sorter]
    return V1_sorted ,V2_sorted
#-----------------------------
def AUC_dose(v):
    V =np.asanyarray(v)
    return V[0]/V[1]
#--------------------
def partition_coeffs(logP_ow, pKa, fup, BP):
   
    pKa2 = 0 
  
    T   = ["Liver"]

    # Water content. 
    Vw  = [ 0.705 ]

    # Neutral lipid content. 
    Vnl = [ 0.0138]

    # Phospholipid content.
    Vph = [ 0.0303]

    # Protein (fup) and tissue (fut) binding.
    fut  = 1/(1 + (((1 - fup)/fup)*0.5)) 

    # Octanol:buffer and Log olive oil:buffer partition coefficient.
    P_ow = math.pow(10, logP_ow) 
    logP_vow = 1.115*logP_ow - 1.35 

    pH = 7.4
    c1 = math.pow(10, pKa - pH) 
    c2 = math.pow(10, pH - pKa2)
    LogD_vow = logP_vow - math.log10(1 + c1 + c2)
    D_vow  = math.pow(10, LogD_vow)

    # Calculation of partition coefficients.
    den  = (P_ow*(Vnl[0] + 0.3*Vph[0]) + (Vw[0] + 0.7*Vph[0]))*fut # Non fat.
    denF = D_vow*(Vnl[0] + 0.3*Vph[0]) + (Vw[0] + 0.7*Vph[0])      # Fat.
    num = (D_vow*(Vnl[0] + 0.3*Vph[0]) + (Vw[0] + 0.7*Vph[0]))*fup
    Ptp = num/denF
            
    pc =Ptp/BP
    
    return  pc
#-------------------

