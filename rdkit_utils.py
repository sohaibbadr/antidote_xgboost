from rdkit import Chem, DataStructs
from urllib.request import urlopen
from urllib.parse import quote
import requests
from rdkit.Chem.Scaffolds import MurckoScaffold
import itertools
from rdkit.Chem import rdRGroupDecomposition as rdRGD
import pandas as pd
#==============================
def unique(list):
    unique_list = []
    for x in list:
        if x not in unique_list:
            unique_list.append(x)
    return unique_list
#==============================
class mol_char():
    # Tanimoto coeff
    def tanimoto_calc(smi1, smi2):
        mol1 = Chem.MolFromSmiles(smi1)
        mol2 = Chem.MolFromSmiles(smi2)
        fp1 = Chem.AllChem.GetMorganFingerprintAsBitVect(mol1, 3, nBits=2048)
        fp2 = Chem.AllChem.GetMorganFingerprintAsBitVect(mol2, 3, nBits=2048)
        s = round(DataStructs.TanimotoSimilarity(fp1,fp2),3)
        return s
    #=============================
    # apply Murcko scaffolds
    def Murcko_Decomposition(smiles):
        cores = []
        for i in range(len(smiles)):
            m = Chem.MolFromSmiles(smiles[i])
            core = MurckoScaffold.GetScaffoldForMol(m)
            cores.append(Chem.MolToSmiles(core))
        df = pd.DataFrame()
        df.insert(loc=0, column="cores", value = cores)
        frame = df["cores"].value_counts().rename_axis('unique_values').reset_index(name='counts')
        Cores = [a for a in frame["unique_values"]]
        if Cores[0] != "":
            most_com = Cores[0]
        else:
            most_com = Cores[1]
        return frame
    #=============================
    # find common core structures
    def core_check(generated_samples, file_path):
        Cores, most_com = mol_char.Murcko_Decomposition(file_path)
        match = []
        gen_cores = []
        for i in range(len(generated_samples)):
            m = Chem.MolFromSmiles(generated_samples[i])
            core = MurckoScaffold.GetScaffoldForMol(m)
            gen_cores.append(Chem.MolToSmiles(core))
        for mol_core in gen_cores:
            if mol_core in most_com:
                match.append(1)
            else:
                match.append(0)
        return match
#===============================
#===============================
#===============================
class mol_inq():
    # fetch SMILES using mol name From CIR
    def CIRconvert(ids):
        try:
            url = 'http://cactus.nci.nih.gov/chemical/structure/' + quote(ids) + '/smiles'
            smiles = urlopen(url).read().decode('utf8')
            return smiles
        except:
            smiles = "Not found"
            return smiles
    #========================
    # fetch SMILES using mol name From pubchem
    def Pubchemconvert(ids):
        try:
            url = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/' + quote(ids) + '/property/CanonicalSMILES/TXT'
            smiles = requests.get(url).text.rstrip()
            if('NotFound' in smiles):
                smiles = "Not found"
            else: 
                return smiles
        except:
            smiles = "Not found"
            return smiles
    #========================
    def name2smiles(name):
        smiles = mol_inq.Pubchemconvert(name)
        if smiles == "Not found":
            smiles = mol_inq.CIRconvert(name)
        if smiles:
            con_smiles = Chem.MolToSmiles(Chem.MolFromSmiles(smiles))
        else:
            con_smiles = "No SMILES found"
        return con_smiles
#===============================
#===============================
#===============================
class mol_process():
    #=====================
    def Sanitize(mols):
        clean_mol = []
        for sample in mols:
            clean = Chem.SanitizeMol(sample)
            if sample:
                clean_mol.append(sample)
            else:
                continue
        return clean_mol
#===============================
#===============================
#===============================
class oxime():
    # find unique R groups within a set of molecules
    def unique_R(rgroups):
        seen = set()
        keep = []
        for rg in rgroups:
            smi = rg
            if smi not in seen:
                keep.append(rg)
                seen.add(smi)
        return keep
    #=========================
    def extend_linker(mol):
        '''linker_3 = []
        linker_4 = []
        linker_5 = []'''
        frags = mol.split("-")
        linker_3 = frags[0] + "CCCO" + frags[2]
        linker_3_ = Chem.MolToSmiles(Chem.MolFromSmiles(linker_3))
        linker_4 = frags[0] + "CCCCO" + frags[2]
        linker_4_ = Chem.MolToSmiles(Chem.MolFromSmiles(linker_4))
        linker_5 = frags[0] + "CCCCCO" + frags[2]
        linker_5_ = Chem.MolToSmiles(Chem.MolFromSmiles(linker_5))    
        return linker_3_,linker_4_,linker_5_
    #=========================
    def generate_new_candidates(core,smiles):
        ms = [Chem.MolFromSmiles(smi) for smi in smiles]
        scaffold1 = Chem.MolFromSmiles(core)
        rgd,failed = rdRGD.RGroupDecompose([scaffold1],ms,asSmiles= True,asRows=False)
    #=====================
        Cores = oxime.unique_R(rgd['Core'])
        rs = []
        if 'R1' in rgd.keys():
            r1s =  oxime.unique_R(rgd['R1'])
        if 'R2' in rgd.keys():
            r2s =  oxime.unique_R(rgd['R2'])
        if "R3" in rgd.keys():
            r3s =  oxime.unique_R(rgd['R3'])
        if "R4" in rgd.keys():
            r4s =  oxime.unique_R(rgd['R4'])
        if "R5" in rgd.keys():
            r5s =  oxime.unique_R(rgd['R5'])
        order = itertools.product(*[r1s,r2s,r3s,r4s,r5s])
        order_ = list(order)
        new = []
        for tpl in order_:
            count = 0
            mol = Cores[0]
            while len(tpl) > count:
                mol = mol + "." + tpl[count]
                count+=1
            new.append(mol)
        new_cand = []
        frag = []
        for i in range(len(new)):
            print(i,new[i])
            try:
                sample = Chem.MolFromSmiles(new[i])
                zipped = Chem.MolToSmiles(Chem.molzip(sample))
                zipped_mol = Chem.MolFromSmiles(zipped)
                frag.append(new[i])
                zipped_rm_H = Chem.RemoveHs(zipped_mol)
                new_cand.append(Chem.MolToSmiles(zipped_rm_H))
            except:
                print("invalid product")
            uni = unique(new_cand)
        return r1s,r2s,r3s,r4s,r5s,new_cand,frag
    #============================
    