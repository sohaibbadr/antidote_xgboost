from random import seed
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import RandomizedSearchCV
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)
import xgboost as xgb
from xgboost import XGBClassifier
import os
from sklearn.preprocessing import LabelEncoder
#=====================
def label_decode(list):
   list_ = []
   for i in range(len(list)):
      if list[i] == 0:
         list_.append("Nontoxic")
      else:
         list_.append("immunotoxic")
   return list_
#=========================
# importing data set 
features_array = []
n_labels = 1
os.chdir("/home/qspt_user/iqsars/src/QSAR_Model/data")
df = pd.read_csv("modified_Immunotixicity_QSAR_tox_score.csv", sep ="\t", header = 0, index_col=[0])
#df = df.drop(['smiles'], axis = 1)
os.chdir("/home/qspt_user/MS projects/msu-qsar/QSAR_project/padelQSAR")
#--------------
#droping the unnecessary columns 
df_features = df.drop(['Relative Toxicity', "Toxicity_class"],axis =1)
print(df_features.shape)
print(df_features.shape)
n_features = df_features.shape[1]
n_features_T = df_features.shape[1] - 100
targeted_fea = n_features - n_features_T
features = np.array(df_features.columns.values)
#=====================
#=====================
# starting training and testing
X = df_features.values
#X = selector.fit_transform(X)
#X = trans.fit_transform(X)
labels = df['Toxicity_class']
#labels = labels.reshape((6343,))
label_encoder = LabelEncoder()
label_encoder = label_encoder.fit(labels)
label_encoded = label_encoder.transform(labels)
mapping_dict = dict(zip(label_encoder.classes_, label_encoder.transform(label_encoder.classes_)))
print(mapping_dict)
#labels = labels.values
#data_dmatrix = xgb.DMatrix(data = X,label=label_encoded,feature_names= features)
X_train, X_test, y_train, y_test = train_test_split(X, label_encoded,test_size = 0.15,random_state = 42 )    
#hyperparameters optimazation
learning_rate =[0.075,0.05,0.025,0.001]
colsample_bytree = [0.1,0.2,0.3]
alpha = [5,10,15]
random_grid ={'learning_rate': learning_rate,
            'colsample_bytree': colsample_bytree,
            'reg_alpha':alpha,
            'n_estimators' :[10]
              }
 #---------------------
 #random forest 
clf_xgb = xgb.XGBClassifier()
clf_random_xgb = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid) # default 5-fold cv
clf_random_xgb.fit(X_train,y_train)
optimized_clf_xgb = clf_random_xgb.best_params_
clf_xgb.set_params(learning_rate=optimized_clf_xgb['learning_rate'],colsample_bytree=optimized_clf_xgb['colsample_bytree'],
           reg_alpha=optimized_clf_xgb['reg_alpha'],n_estimators=optimized_clf_xgb['n_estimators']
              )
clf_xgb.fit(X_train,y_train)
importance_ = clf_xgb.feature_importances_
# end of regressor training
RF_train = clf_xgb.predict(X_train)
RF_train =[a for a in RF_train]
RF_test = clf_xgb.predict(X_test)
RF_test = [a for a in RF_test]
#------------------
features_RF_sorted,features_sorted = ACATlib.ACAT_Sorter(importance_ ,features)
features_sorted = np.flip(features_sorted)
features_sorted_T = features_sorted[n_features_T:n_features]
features_RF_sorted_T = features_RF_sorted[n_features_T:n_features]
features_array.append(features_sorted_T)
features_RF_sorted_T = [ a for a in features_RF_sorted_T]
#---------------------
# Reduced training 
seed(1)
sequence = [i for i in range(1,n_features+1,150)]
rr = np.asarray(sequence)
rr =rr[1:200]
ACC_r = np.zeros(len(rr))
for i in range(len(rr)): 

   Reduced = features_sorted[0:rr[i]]
   Reduced =[a for a in Reduced]
   df_features_R = df_features[Reduced]
   XR = df_features_R.values
   #XR = selector.fit_transform(XR)
   #XR = trans.fit_transform(XR)
   #data_dmatrix_R = xgb.DMatrix(data=X,label=label_encoded)
   XR_train, XR_test, yR_train, yR_test = train_test_split(XR, label_encoded,test_size = 0.15,random_state = 42 )
   clf_xgb_R = xgb.XGBClassifier()
   clf_random_xgb_R = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid)
   clf_random_xgb_R.fit(XR_train,yR_train)
   optimized_clf_xgb_R = clf_random_xgb_R.best_params_
   clf_xgb_R.set_params(learning_rate=optimized_clf_xgb_R['learning_rate'],colsample_bytree=optimized_clf_xgb_R['colsample_bytree'],
           reg_alpha=optimized_clf_xgb_R['reg_alpha'],n_estimators=optimized_clf_xgb_R['n_estimators']
              )
   clf_xgb_R.fit(XR_train,yR_train)
   RF_train_R = clf_xgb_R.predict(XR_train)
   RF_test_R = clf_xgb_R.predict(XR_test)
   ACC_r[i] = accuracy_score(yR_test, RF_test_R)
#---------------------------------------
lower_mse = np.max(ACC_r)
best_feature = np.argmax(ACC_r)
best_feature = rr[best_feature]
#best training&testing 
Reduced = features_sorted[0:best_feature]
Reduced =[a for a in Reduced]
df_features_R = df_features[Reduced]
XR = df_features_R.values

#data_dmatrix_R = xgb.DMatrix(data=X,label=label_encoded)
XR_train, XR_test, yR_train, yR_test = train_test_split(XR, label_encoded,test_size = 0.15,random_state = 42 )
clf_xgb_R = xgb.XGBClassifier()
clf_random_xgb_R = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid)
clf_random_xgb_R.fit(XR_train,yR_train)
optimized_clf_xgb_R = clf_random_xgb_R.best_params_
clf_xgb_R.set_params(learning_rate=optimized_clf_xgb_R['learning_rate'],colsample_bytree=optimized_clf_xgb_R['colsample_bytree'],
        reg_alpha=optimized_clf_xgb_R['reg_alpha'],n_estimators=optimized_clf_xgb_R['n_estimators']
            )
clf_xgb_R.fit(XR_train,yR_train)
RF_train_R = clf_xgb_R.predict(XR_train)
RF_test_R = clf_xgb_R.predict(XR_test)

#========================================
print(Reduced)
print(lower_mse)
#y_pred = clf_xgb_R.predict()
cm = confusion_matrix(label_decode(yR_test), label_decode(RF_test_R))
#cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
print(cm)
print(classification_report(label_decode(yR_test), label_decode(RF_test_R)))
'''os.chdir("/home/qspt_user/data_efficient_grammar/Pretrained_models")
pickle.dump(clf_xgb_R, open("Immuno_class.pkl", "wb"))'''
#============================


