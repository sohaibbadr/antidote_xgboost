from random import seed
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split , cross_val_score
from sklearn.datasets import make_regression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
import pickle
import matplotlib.pyplot as plt
import ACATlib
import importlib
importlib.reload(ACATlib)
import xgboost as xgb
from xgboost import plot_tree
#---------------
# importing the data set 
n_features = 3652
n_features_T = 3600
targeted_fea = n_features - n_features_T
features_array = []
n_labels = 1
df = pd.read_csv('nerve_agents_full.csv',sep=',',header = 0)
df = df.drop(['Unnamed: 0'], axis = 1)
print(df.shape)
#--------------
#droping the unnecessary columns 
df_features = df.drop(['%Reactivation'],axis =1)
features = np.array(df_features.columns.values)
#=====================
#=====================
# starting training and testing
X = df_features.values
labels = df['%Reactivation']
labels = labels.values
labels = labels.reshape((240,))
data_dmatrix = xgb.DMatrix(data=X,label=labels,feature_names= features)
X_train, X_test, y_train, y_test = train_test_split(X, labels,test_size = 0.3,random_state = 42 )    
#hyperparameters optimazation
learning_rate =[0.1,0.25,0.3]
colsample_bytree = [0.1,0.2,0.3]
alpha = [5,10,15]
random_grid ={'learning_rate': learning_rate,
            'colsample_bytree': colsample_bytree,
            'alpha':alpha,
            'n_estimators' :[1]
              }
 #---------------------
 #random forest 
clf_xgb = xgb.XGBRegressor()
clf_random_xgb = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid)
clf_random_xgb.fit(X_train,y_train)
optimized_clf_xgb = clf_random_xgb.best_params_
clf_xgb.set_params(learning_rate=optimized_clf_xgb['learning_rate'],colsample_bytree=optimized_clf_xgb['colsample_bytree'],
           alpha=optimized_clf_xgb['alpha'],n_estimators=optimized_clf_xgb['n_estimators']
              )
clf_xgb.fit(X_train,y_train)
importance_ = clf_xgb.feature_importances_
# end of regressor training
RF_train = clf_xgb.predict(X_train)
RF_train =[a for a in RF_train]
RF_test = clf_xgb.predict(X_test)
RF_test = [a for a in RF_test]
#------------------
features_RF_sorted,features_sorted = ACATlib.ACAT_Sorter(importance_ ,features)
features_sorted = np.flip(features_sorted)
features_sorted_T = features_sorted[n_features_T:n_features]
features_RF_sorted_T = features_RF_sorted[n_features_T:n_features]
features_array.append(features_sorted_T)
features_RF_sorted_T = [ a for a in features_RF_sorted_T]
#---------------------
# Reduced training 
seed(1)
sequence = [i for i in range(1,n_features+1,100)]
rr = np.asarray(sequence)
rr =rr[1:3652]
ACC_r = np.zeros(len(rr))
for i in range(len(rr)): 

   Reduced = features_sorted[0:rr[i]]
   Reduced =[a for a in Reduced]
   df_features_R = df_features[Reduced]
   XR = df_features_R.values
   data_dmatrix_R = xgb.DMatrix(data=X,label=labels)
   XR_train, XR_test, yR_train, yR_test = train_test_split(XR, labels,test_size = 0.35,random_state = 42 )
   clf_xgb_R = xgb.XGBRegressor()
   clf_random_xgb_R = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid)
   clf_random_xgb_R.fit(XR_train,yR_train)
   optimized_clf_xgb_R = clf_random_xgb_R.best_params_
   clf_xgb_R.set_params(learning_rate=optimized_clf_xgb_R['learning_rate'],colsample_bytree=optimized_clf_xgb_R['colsample_bytree'],
           alpha=optimized_clf_xgb_R['alpha'],n_estimators=optimized_clf_xgb_R['n_estimators']
              )
   clf_xgb_R.fit(XR_train,yR_train)
   RF_train_R = clf_xgb_R.predict(XR_train)
   RF_test_R = clf_xgb_R.predict(XR_test)
   ACC_r[i] = r2_score(yR_test, RF_test_R)
#---------------------------------------
lower_mse = np.max(ACC_r)
best_feature = np.argmax(ACC_r)
best_feature = rr[best_feature]
#best training&testing 
Reduced = features_sorted[0:best_feature]
Reduced =[a for a in Reduced]
df_features_R = df_features[Reduced]
XR = df_features_R.values
data_dmatrix_R = xgb.DMatrix(data=X,label=labels)
XR_train, XR_test, yR_train, yR_test = train_test_split(XR, labels,test_size = 0.35,random_state = 42 )
clf_xgb_R = xgb.XGBRegressor()
clf_random_xgb_R = RandomizedSearchCV(estimator = clf_xgb, param_distributions = random_grid)
clf_random_xgb_R.fit(XR_train,yR_train)
optimized_clf_xgb_R = clf_random_xgb_R.best_params_
clf_xgb_R.set_params(learning_rate=optimized_clf_xgb_R['learning_rate'],colsample_bytree=optimized_clf_xgb_R['colsample_bytree'],
        alpha=optimized_clf_xgb_R['alpha'],n_estimators=optimized_clf_xgb_R['n_estimators']
            )
clf_xgb_R.fit(XR_train,yR_train)
RF_train_R = clf_xgb_R.predict(XR_train)
RF_test_R = clf_xgb_R.predict(XR_test)
#========================================
#========================================
# starting data analysis and visualazation
error_ = np.zeros(len(yR_train))
for i in range(len(yR_train)):
   error_[i] = (((yR_train[i]-RF_train_R[i])/yR_train[i]))
error_ = [a for a in error_]
upper = (yR_train/yR_train)*0.1
upper = [a for a in upper]
lower = (yR_train/yR_train)*-0.1
lower = [a for a in lower]
r = np.linspace(0,len(yR_train),num = len(yR_train))
r = [a for a in r]
BINS = np.linspace(-1,1,21)
BINS = [x for x in BINS]
#--------------------------------------
plt.figure(1)
plt.plot(r,error_,'b.', label = 'perfect prediction')
plt.plot(r,upper, label = '+ 10 % prediction')
plt.plot(r,lower, label = '- 10 % prediction')
plt.xlabel('Sample',fontsize = 18)
plt.ylabel('Fractional error in prediction',fontsize = 18)
plt.ylim((-2,2))
plt.title(' figure 2 : Nerve agent prediction_xgb',fontsize = 18)
plt.grid() 
print(lower_mse,best_feature) 
#print(features_RF_sorted_T)
#print(features_sorted_T)
#-----------------------
#-----------------------
plt.figure(2)
plt.hist(error_,bins =BINS, color = 'blue',ec ='black',density =1)
plt.xlabel('Fractional Error',fontsize = 18)
plt.ylabel('Probability*10',fontsize = 18)
plt.xticks(np.asanyarray(BINS))
plt.xticks(rotation = 90)
plt.title('Figure 3 : probability of getting a fractional error from RF',fontsize = 18)
plt.grid()
feature_count = np.arange(0,targeted_fea,1)
features_RF_sorted_T = np.flip(features_RF_sorted_T)
#-----------------------
plt.figure(3)
plt.bar(feature_count,features_RF_sorted_T, label = 'perfect prediction')
plt.legend()
plt.xlabel('FEature')
plt.ylabel('importance')
plt.title('Feature importance')
plt.grid()
plt.show()
#-----------------------
plt.figure(4)
plot_tree(clf_xgb,num_trees= 0, rankdir='LR')
plt.show()   
#=============================
df_feature_imp = pd.read_csv('morder_desc.csv',sep=',',header = 0)
appr = df_feature_imp['name']
desc = df_feature_imp['description']
appr = np.asarray(appr)
appr = [ a for a in appr]
desc = np.asarray(desc)
desc = [ a for a in desc]
'''#=============================
belongs_2 = []
description = []
count_nerve = 0
count_Oxime = 0
for L in range(len(features_sorted_T)):
      if '*' in features_sorted_T[L] :
         belongs_2.append('nerve agent')
         features_sorted_T[L] = features_sorted_T[L].replace('*','')
         index = appr.index(features_sorted_T[L])
         description.append(desc[index])
         count_nerve =count_nerve + 1
      else : 
         belongs_2.append('Oxime')
         index = appr.index(features_sorted_T[L])
         description.append(desc[index])
         count_Oxime = count_Oxime + 1
c_1 = np.asarray(features_sorted_T) 
c_2 = np.asarray(belongs_2)
c_3 = np.asarray(description)
imp_feature = np.array([c_1,c_2,c_3])
imp_feature = np.transpose(imp_feature)
imp_feature = pd.DataFrame(imp_feature)
imp_feature.columns = [['Descriptor','For','Description']]
imp_feature.to_csv('MSU_imp_features.csv')
print(imp_feature.tail())
print(count_nerve,count_Oxime)
#------------------------
#predicting new nerve agent 
nerve_agents_csv = np.array(['nerve_test_PIMP_full.csv','nerve_test_NEMP_full.csv','nerve_test_PXN_full.csv','nerve_test_DFP_full.csv'])
RF_new = np.zeros((25,len(nerve_agents_csv)))
for i in range(len(nerve_agents_csv)):
   df__ = pd.read_csv(nerve_agents_csv[i],sep=',',header = 0)
   df__.fillna(0, inplace=True)
   X_new = df__[Reduced].values
   RF_new[:,i] = clf_xgb_R.predict(X_new)
RF_new = pd.DataFrame(RF_new)
RF_new.columns = [['PIMP','NEMP','PXN','DFP']]    
df2 = pd.read_csv('labels.csv',sep=',',header = 0)
df_id = pd.read_csv('qsar_out.csv',sep=',',header = 0)
#n_0 = df_id['_id']
n_1 = RF_new[['PIMP']]
n_1.columns = ['PIMP']
n_2 = RF_new[['NEMP']]
n_2.columns = ['NEMP']
n_3 = RF_new[['PXN']]
n_3.columns = ['PXN']
n_4 = RF_new[['DFP']]
n_4.columns = ['DFP']
#------------------------------------
nerve_agents1_csv = np.array(['nerve_test_NIMP_full.csv','nerve_test_VX_full.csv','nerve_test_NEDPA_full.csv','nerve_test_diazinon_full.csv',
'nerve_test_chlorpyrifos_full.csv','nerve_test_Phorate_oxon_full.csv','nerve_test_Sarin_full.csv','nerve_test_Meta_full.csv'])
RF_new1 = np.zeros((85,len(nerve_agents1_csv)))
for i in range(len(nerve_agents1_csv)):
   df__ = pd.read_csv(nerve_agents1_csv[i],sep=',',header = 0)
   df__.fillna(0, inplace=True)
   X_new1 = df__[Reduced].values
   RF_new1[:,i] = clf_xgb_R.predict(X_new1)
RF_new1 = pd.DataFrame(RF_new1)
RF_new1.columns = [['NIMP','VX','NEDPA','diazinon','chlorpyrifos','Phorate_oxon','Sarin','Meta']]    
df2 = pd.read_csv('labels.csv',sep=',',header = 0)
df_id = pd.read_csv('qsar_out.csv',sep=',',header = 0)
n_5 = RF_new1[['NIMP']]
n_5.columns = ['NIMP']
n_6 = RF_new1[['VX']]
n_6.columns = ['VX']
n_7 = RF_new1[['NEDPA']]
n_7.columns = ['NEDPA']
n_8 = RF_new1[['diazinon']]
n_8.columns = ['diazinon']
n_9 = RF_new1[['chlorpyrifos']]
n_9.columns = ['chlorpyrifos']
n_10 = RF_new1[['Phorate_oxon']]
n_10.columns = ['Phorate_oxon']
n_11 = RF_new1[['Sarin']]
n_11.columns = ['Sarin']
n_12 = RF_new1[['Meta']]
n_12.columns = ['Meta']
nerve_agent = pd.concat([n_1,n_2,n_3,n_4], axis=1, sort=False)
nerve_agent1 = pd.concat([n_5,n_6,n_7,n_8,n_9,n_10,n_11,n_12], axis=1, sort=False)
nerve_agent.to_csv('xg_predict_1_lg.csv')
nerve_agent1.to_csv('xg_predict_2_lg.csv')
print(nerve_agent.tail())
print(nerve_agent.shape)
print(nerve_agent1.tail())
print(nerve_agent1.shape)
#----------------------------
#negative control 
#------------------------------------
nerve_agents1_csv = np.array(['negative_control.csv'])
RF_new1 = np.zeros((8,len(nerve_agents1_csv)))
for i in range(len(nerve_agents1_csv)):
   df__ = pd.read_csv(nerve_agents1_csv[i],sep=',',header = 0)
   df__.fillna(0, inplace=True)
   X_new1 = df__[Reduced].values
   RF_new1[:,i] = clf_xgb_R.predict(X_new1)
RF_new1 = pd.DataFrame(RF_new1)
print(RF_new1)'''
